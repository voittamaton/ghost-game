const SKYNET_WINS = 1;
const HUMAN_LIAR = 2;


var LineByLineReader = require('line-by-line'),
    lr,
    fs = require('fs'),
    //tree = {},
    winnerWords = [],
    loserWords = [],
    imWinning = true;

Array.prototype.findReg = function(match) {
    var reg = new RegExp(match);

    return this.filter(function(item){
        return typeof item == 'string' && item.match(reg);
    });
}

var Library = {
    utils: {
        /*
         * At first I thought to tackle the problem transforming the wordlist in a tree, but then I thought that it
         * would take me more time.
         *

        addWord: function(word) {
            var current = tree;
            var chars = word.split('');
            for (var i = 0; i < chars.length; i++) {
                if (typeof current[chars[i]] === 'undefined' ) {
                    current[chars[i]] = {};
                }
                current = current[chars[i]];
            }
            current.end = true;
        }*/
        addWord: function(word, firstChar){
            if(word[0] === firstChar) {

                /* We will only need the words that start with the letter given by the player
                 * Doing this we decrease the size of the object and the execution time
                 */
                //Library.utils.addWord(line);
                if(word.length%2 === 0) { // no ganadoras
                    loserWords.push(word);
                } else {
                    winnerWords.push(word);
                }
            } else if(word[0] === String.fromCharCode(firstChar.charCodeAt(0) + 1)) { // para el proceso en caso de que la letra con la que empieza la palabra sea la siguiente letra a la letra elegida
                lr.close();
            }
        },
        longestWords: function(array, callback){ // return the longest word in an array
            var a = 0, b = 0, c = 0, i = array.length;
            if(i) while (i--) {
                b = array[i].length;
                if (b > a) {
                    c = i;
                    a = b;
                }
            }
            callback(array[c]);
        }
    }
};

(function() {
    module.exports = {
        load : function(fileName, firstChar, callback) {
            lr = new LineByLineReader(__dirname + '/inc/wordLists/'+fileName);
            lr.on('error', function (err) {
                return callback(err);
            });
            lr.on('line', function (line) {
                if(line.length >= 4) {
                    Library.utils.addWord(line, firstChar);
                }
            });
            lr.on('end', function () {
                /*
                 * When the words are all loaded, it has to remove every winner word that start with a loser word with four or more letters.
                 */
                loserWords.filter(function(element){
                    if(element.length >= 4) {
                        var winWord = winnerWords.findReg("^" + element);
                        if (winWord.length > 0) {
                            for (var i = 0; i < winWord.length; i++) {
                                winnerWords.splice(winnerWords.indexOf(winWord[i]), 1);
                                console.log("Borrado" + winWord[i]);

                            }
                        }
                    }
                });
                return callback(null);
            });
        },
        selectChar : function(chars, callback) {
            var promiseWinnerWords = new Promise(function(resolve, reject){
                resolve(winnerWords = winnerWords.filter(function(element){
                    if (element.lastIndexOf(chars, 0) === 0) {
                        return element;
                    }
                }));
            });

            var promiseLoserWords = new Promise(function(resolve, reject){
                resolve(loserWords = loserWords.filter(function(element){
                    if (element.lastIndexOf(chars, 0) === 0) {
                        return element;
                    }
                }));
            });
            promiseLoserWords.then(function(){
                promiseWinnerWords.then(function(){
                    var random = Math.floor(Math.random() * winnerWords.length);
                    if(winnerWords.indexOf(chars) > -1 && chars.length >= 4) {
                        console.log("Entra");
                        return callback(null, imWinning, null, SKYNET_WINS);
                    }
                    if(typeof winnerWords[random] === 'undefined') { // There are not coincidences
                        var slowLoseOpt = [];
                        if(loserWords.length > 0) {
                            slowLoseOpt = loserWords.findReg("^" + chars);
                        }

                        switch(slowLoseOpt.length){
                            case 0:
                                return callback(null, imWinning, null, HUMAN_LIAR);
                                break;
                            default:
                                /*
                                 * Before we have to remove all the words with char+1 letters and all that starts with those words
                                 */
                                var nope = []; //banned words
                                var longest = slowLoseOpt.filter(function(element){
                                    if (element.length === chars.length + 1) {
                                        nope = nope.concat(slowLoseOpt.findReg("^" + element));
                                    }
                                    console.log(nope);
                                    return nope.indexOf(element) === -1
                                });
                                if(longest.length === 0) { // we return the last char. Game lost for skynet.
                                    longest = [nope[0]]; // nope[0] will always be a word with chars+1 characters
                                }
                                winnerWords = longest;
                                imWinning = false;
                                break;
                        }
                    }

                    var selectedChar = "";
                    if(imWinning) {
                        selectedChar = winnerWords[random][chars.length];
                    } else {
                        Library.utils.longestWords(winnerWords, function(word){
                            selectedChar = word[chars.length];
                        })
                    }
                    chars+=selectedChar;
                    var promiseWinnerWordsBefore = new Promise(function(resolve, reject){
                        resolve(winnerWords = winnerWords.filter(function(element){
                            if (element.lastIndexOf(chars, 0) === 0) {
                                return element;
                            }
                        }));
                    });

                    var promiseLoserWordsBefore = new Promise(function(resolve, reject){
                        resolve(loserWords = loserWords.filter(function(element){
                            if (element.lastIndexOf(chars, 0) === 0) {
                                return element;
                            }
                        }));
                    });
                    promiseLoserWordsBefore.then(function(){
                        promiseWinnerWordsBefore.then(function(){
                            fs.writeFile("winner.txt", winnerWords, function(err) {
                                if(err) {
                                    return callback(err);
                                }
                                console.log("Winner saved");
                            });
                            fs.writeFile("loser.txt", loserWords, function(err) {
                                if(err) {
                                    return callback(err);
                                }
                                console.log("Loser saved");
                            });
                            return callback(selectedChar, imWinning, winnerWords);
                        });
                    });
                });
            });
        }
    }
}());
