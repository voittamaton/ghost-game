angular.module('ghostGame', [])
    .controller("mainController", ["$scope", "$http", function($scope, $http){
        $scope.formData = {};
        $scope.chars = "";
        $scope.message = "";

        $scope.sendChar = function() {
            document.querySelector('#btnAdd').disabled = true;
            $scope.chars += $scope.char.text;
            $http.post('/api/char', $scope.char)
                .success(function(data){
                    document.querySelector('#btnAdd').disabled = false;
                    if(typeof data === 'object') {
                        console.log(data);
                        if (data.char === "null") {
                            $scope.chars.slice(0, -1);
                        } else {
                            $scope.chars += "-"+data.char;
                        }
                        $scope.message = data.message;
                        document.querySelector('#btnRestart').style.visibility = 'visible';
                        document.querySelector('#btnAdd').disabled = true;
                    } else {
                        $scope.char = {};
                        $scope.chars += '-' + data + '-';
                    }
                })
                .error(function(data){
                    console.log('Error:' + data);
                });
        },
        $scope.restart = function() {
            $http.post('/api/restart', null)
                .success(function(response){
                    if(response === 'OK') {
                        document.querySelector('#btnRestart').style.visibility = 'hidden';
                        document.querySelector('#btnAdd').disabled = false;
                        $scope.formData = {};
                        $scope.chars = "";
                        $scope.message = "";
                        document.querySelector('#txtChar').value = '';
                    }
                })
                .error(function(response){
                    console.log('Error:' + response);
                });
        }
    }]);
