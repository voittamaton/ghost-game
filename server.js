/*
 * Main file for the node server. It includes the endpoints used in the front-end and Express to
 * load the content.
 *
 * Endpoints:
 *
 * /api/char: it receives a character and gives back another one.
 * /api/restart: it cleans all the necessary variables in order to start a new game.
 *
 */

var express = require('express'),
    expressLogger = require('express-logger'),
    logger = require('./logger'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    app = express(),
    library = require("./library.js"),
    wordLibrary = "word.lst", // The default one
    tree = {},
    chars = '',
    freeport = require('freeport');
//File localization
app.use(express.static(__dirname + '/public'));
// Show a log of every request in the terminal console
app.use(expressLogger({path: "logfile.txt"}));
//Change html
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
//For delete and put API methods
app.use(methodOverride());

function startServer(){
    logger.log('info', 'Starting server. Checking for a free port...');
    freeport(function(err, port){
        if (err) throw err;
        logger.log('info', 'Port ' + port + ' is free!');
        app.listen(port, function(){
            logger.log('info', 'Server listening on port '+ port);
            logger.log('info', 'http://localhost:' + port);
        });
    });
}

if(typeof process.argv[2] === "undefined") {
    logger.log('info', 'Using default worldlist');
} else {
    wordLibrary = process.argv[2];
    logger.log('Using ' + process.argv[2] + ' wordlist');
}
startServer();

// Paths
// Starts the game with the given character

app.post('/api/char', function(req, res) {
    chars += req.body.text;
    if(chars.length === 1) {
        library.load(wordLibrary, req.body.text, function(err) {
            if (err) {
                console.log("File not found");
                process.exit();
            } else {
                library.selectChar(chars, function(char){
                    chars+=char;
                    res.send(char);
                });
            }
        });
    } else {
        library.selectChar(chars, function(char, imWinning, winerWords, gameStatus){
            chars+=char;

            if(!imWinning && winerWords.length === 1 && winerWords[0].length === chars.length) {
                res.send('{"char" : "'+char+'", "message" : "Skynet lose"}');
            } else if(gameStatus) {
                switch (gameStatus) {
                    case 1:
                        res.send('{"char" : "'+char+'", "message" : "Skynet wins"}');
                        console.log("La máquina gana");
                        break;
                    case 2:
                        res.send('{"char" : "'+char+'", "message" : "Human lied. Skynet wins"}');
                        break;
                }
            } else {
                res.send(char);
            }
        });
    }
});

// Restart the game
app.post('/api/restart', function(req, res) {
    //Cleaning the 'chars' variable the game will start again in the next POST to /api/char
    chars = '';
    res.sendStatus(200);
});

//Loads the html view
app.get('*', function(req, res){
    res.sendFile(__dirname + '/public/index.html');
});
